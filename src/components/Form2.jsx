import React, {useState, useEffect}  from 'react';
import Collect from '../services/Collect';

const Form2 = () => {
                
    const [houseNo, setHouseNo] = useState();
    const [street, setStreet] = useState();
    const [city, setCity] = useState();
    const [zipCode, setZipCode] = useState();

    useEffect(() => {
        const _houseNo = localStorage.getItem('houseNo');
        const _street = localStorage.getItem('street');
        const _city = localStorage.getItem('city');
        const _zipCode = localStorage.getItem('zipCode');
        if (_houseNo) 
            setHouseNo(_houseNo);
        if (_street) 
            setStreet(_street);
        if (_city) 
            setCity(_city);
        if (_zipCode)
            setZipCode(_zipCode);
      },[]);
      

    function handleHouseNoChange(e) {
        setHouseNo(e.target.value)
        Collect.set('houseNo', e.target.value);
    } 

    function handleStreetChange(e) {
        setStreet(e.target.value);
        Collect.set('street', e.target.value);
    }
    
    function handleCityChange(e) {
        setCity(e.target.value);
        Collect.set('city', e.target.value);
    }

    function handleZipCodeChange(e) {
        setZipCode(e.target.value);
        Collect.set('zipCode', e.target.value);
    }

    return (
        <div>
            <h5>Address Information</h5>
            <form>
                <label htmlFor="houseNo">House Number:</label>
                <input
                    required
                    type='text'
                    name='houseNo'
                    value={houseNo}
                    onChange={handleHouseNoChange}
                />
                <label htmlFor="street">Street:</label>
                <input
                    required
                    type='text'
                    name='street'
                    value={street}
                    onChange={handleStreetChange}
                />
                <label htmlFor="city">City:</label>
                <input
                    required
                    type='text'
                    name='city'
                    onChange={handleCityChange}
                    value={city}
                />
                <label htmlFor="zipCode">Zip Code:</label>
                <input
                    required
                    type='text'
                    name='zipCode'
                    onChange={handleZipCodeChange}
                    value={zipCode}
                />
            </form>
        </div>
        
    )
}

export default Form2;