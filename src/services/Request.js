import axios from 'axios';
const baseUrl = 'http://localhost:8080';

//A SIMPLE REQUEST SERVICE WRAPPER ONLY GOOD FOR SUNNY DAYS ;-)
class Request {
  constructor() {
    let service = axios.create({});
    service.interceptors.request.use(async (config) => {
      return config
    })
    this.service = service;
  }
  post(uri,data ,callback, error) {
    console.log(data);
    return this.service.request({
      method: 'POST',
      url: `${baseUrl}/${uri}`,
      data: data,
      responseType: 'json',
    }).then((response) => callback(response.data))
    .catch(error);
  }
}

export default new Request();
