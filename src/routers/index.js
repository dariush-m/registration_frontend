import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Register from '../routers/register'

const Application = () => (
  <Router>
    <React.Fragment>
      <Route exact path="/" component={Register} />
    </React.Fragment>
  </Router>
)

export default Application