import React, { Component } from 'react';
import Application from './routers/index';

class App extends Component {
  render() {
    return (
        <Application/>
    );
  }
}

export default App;
