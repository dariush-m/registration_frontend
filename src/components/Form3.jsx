import React, {useState, useEffect}  from 'react';
import Collect from '../services/Collect';

const Form3 = () => {
                
    const [accountOwner, setAccountOwner] = useState();
    const [iban, setIban] = useState();

    useEffect(() => {
        const _accountOwner = Collect.get('accountOwner');
        const _iban = Collect.get('iban');

        if (_accountOwner) 
            setAccountOwner(_accountOwner);
        if (_iban) 
            setIban(_iban);
      },[]);
      

    function handleAccountOwnerChange(e) {
        setAccountOwner(e.target.value)
        Collect.set('accountOwner', e.target.value);
    } 

    function handleIbanChange(e) {
        setIban(e.target.value);
        Collect.set('iban', e.target.value);
    }
    
    return (
        <div>
            <h5>Payment Information</h5>
            <form>
                <label htmlFor="accountOwner">Account Owner:</label>
                <input
                    required
                    type='text'
                    name='accountOwner'
                    value={accountOwner}
                    onChange={handleAccountOwnerChange}
                />
                <label htmlFor="iban">Iban:</label>
                <input
                    required
                    type='text'
                    name='iban'
                    value={iban}
                    onChange={handleIbanChange}
                />
            </form>
        </div>
    )
}

export default Form3;