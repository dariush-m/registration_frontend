

class Collect {

  get(key){
    return localStorage.getItem(key);
  }

  set(key, val){
    localStorage.setItem(key, val);
  }

  getUser(){
    return {
      firstname : this.get('firstname'),
      lastname : this.get('lastname'),
      telephone: this.get('telephone'),
      email: this.get('email')
    }
  }

  getAddress(){
    return {
      houseNo : this.get('houseNo'),
      street : this.get('street'),
      city: this.get('city'),
      zipCode: this.get('zipCode')
    }
  }

  getPayment(){
    return {
      accountOwner: this.get('accountOwner'),
      iban: this.get('iban')
    }
  }

  getRegister(){
    return {
      user : this.getUser(),
      address: this.getAddress(),
      payment : this.getPayment()
    }
  }
  
}

export default new Collect();
