import React, {useState, useEffect} from 'react';
import request from '../../services/Request';
import Collect from '../../services/Collect';
import Validate from '../../services/Validate';
import Form1 from '../../components/Form1';
import Form2 from '../../components/Form2';
import Form3 from '../../components/Form3';
import Form4 from '../../components/Form4';
import './index.scss';

const RegisterContainer = () => {
  const [step, setStep] = useState(0);
  const [stepStatus, setStepStatus] = useState(false);

  useEffect(() => {
    if ( stepStatus  ) 
      Collect.set('step', step);
  }, [step, stepStatus]);

  useEffect(()=>{
    setStepStatus(true);
    const _step = Collect.get('step');
    if (_step) 
      setStep(Number(_step));
  },[]);

  function handlerNextChange() {
    if(!Validate.steps(step))
      alert('Please fill out all the inputs');
    else if (step === 2)
      submit();
    else
      setStep(step+1);
  }
  const submit = ()=>{
    request.post('api/registers', Collect.getRegister(), (response)=>{
      Collect.set('response', JSON.stringify(response));
      setStep(step+1)
    },(error)=>{
      alert('ERROR, Please Try later!');
      setStep(step+1)
    });
  }

  return (
    <div className='wrapper'>
      <div className='step-container'>
        {step === 0 && <Form1/>}
        {step === 1 && <Form2/>}
        {step === 2 && <Form3/>}
        {step === 3 && <Form4/>}

        <div className="step-button">
          <button 
          className={( step >= 3) ? 'display-none' : null} 
          type='button' onClick={handlerNextChange}>{(step === 2)? "Submit": "Next"}</button>
          <button 
          className={(step === 0 || step >= 3) ? 'display-none' : null} 
          type='button' onClick={()=>{setStep(step-1)}}>Back</button>
        </div>
      </div>
    </div>
  )
}

export default RegisterContainer;

