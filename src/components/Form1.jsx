import React, {useState, useEffect}  from 'react';
import Collect from '../services/Collect';

const Form1 = () => {
                
    const [firstname, setFirstname] = useState();
    const [lastname, setLastname] = useState();
    const [telephone, setTelephone] = useState();
    const [email, setEmail] = useState();

    useEffect(() => {
        const _firstname = Collect.get('firstname');
        const _lastname = Collect.get('lastname');
        const _telephone = Collect.get('telephone');
        const _email = Collect.get('email');
        if (_firstname) 
            setFirstname(_firstname);
        if (_lastname) 
            setLastname(_lastname);
        if (_telephone) 
            setTelephone(_telephone);
        if (_email)
            setEmail(_email);
      },[]);
      
    function handleFirstNameChange(e) {
        setFirstname(e.target.value)
        Collect.set('firstname', e.target.value);
    } 

    function handleLastNameChange(e) {
        setLastname(e.target.value);
        Collect.set('lastname', e.target.value);
    }
    
    function handleTelephoneChange(e) {
        setTelephone(e.target.value);
        Collect.set('telephone', e.target.value);
    }

    function handleEmailChange(e) {
        setEmail(e.target.value);
        Collect.set('email', e.target.value);
    }

    return (
        <div>
            <h5>Personal Information</h5>
            <form>
                <label htmlFor="firstname">First name:</label>
                <input
                    required
                    type='text'
                    name='firstname'
                    value={firstname}
                    onChange={handleFirstNameChange}
                />
                <label htmlFor="lastname">Last name:</label>
                <input
                    required
                    type='text'
                    name='lastname'
                    value={lastname}
                    onChange={handleLastNameChange}
                />
                <label htmlFor="telephone">Telephone:</label>
                <input
                    required
                    type='tel'
                    name='telephone'
                    onChange={handleTelephoneChange}
                    value={telephone}
                />
                <label htmlFor="email">Email:</label>
                <input
                    required
                    type='email'
                    name='email'
                    onChange={handleEmailChange}
                    value={email}
                />
            </form>
        </div>
    )
}

export default Form1;