import Collect from './Collect';
class Validate{
    steps(step){
        if (step === 0)
            return this.user();
        if (step === 1)
            return this.address();
        if (step === 2)
            return this.payment();

        // switch(step){
        //     case 0:
        //         return this.user();
        //     case 1:
        //         return this.address();
        //     case 2:
        //         return this.payment();
        // }
    }
    
    user(){
        let user = Collect.getUser();
        return (user.firstname && user.lastname && user.telephone && user.email);
    }

    address(){
        let address = Collect.getAddress();
        return (address.houseNo && address.street && address.city && address.zipCode);
    }

    payment(){
        let payment = Collect.getPayment();
        return (payment.accountOwner && payment.iban);
    }
}

export default new Validate();