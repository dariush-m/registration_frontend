import React, {useState, useEffect}  from 'react';
import Collect from '../services/Collect';

const Form4 = () => {
    const [response, setResponse] = useState();
    
    useEffect(() => {
        const _response = JSON.parse(Collect.get('response'));
        if (_response)
            setResponse(_response.paymentDataId)
    },[]);

    return (
        <div>
            <h5>You have successfully registerd! </h5>
            <p>Your payment Data Id is:</p>
            <div className='wrap'>{response}</div>
        </div>
    )
}

export default Form4;